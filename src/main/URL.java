public class URL {
    private final String protocol;
    private final String domain;
    private final String path;

    public URL() {
        this.protocol = "http";
        this.domain = "127.0.0.1";
        this.path = "/";
    }

    public URL( String protocol, String domain, String path ) {
        this.protocol = protocol;
        this.domain = domain;
        this.path = path;
    }

    public String getProtocol() { return this.protocol; }
    public String getDomain() { return this.domain; }
    public String getPath() { return this.path; }
}
