import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class URLTest {
    @Test
    public void testDefaultURLProtocol() {
        URL testURL = new URL();
        assertEquals( "Default protocol is not http", "http", testURL.getProtocol() );
    }
    @Test
    public void testDefaultURLDomain() {
        URL testURL = new URL();
        assertEquals( "Default domain is not 127.0.0.1", "127.0.0.1", testURL.getDomain() );
    }
    @Test
    public void testDefaultURLPath() {
        URL testURL = new URL();
        assertEquals( "Default path is not /", "/", testURL.getPath() );
    }
    @Test
    public void testTripletURLProtocol() {
        URL testURL = new URL( "blezzaraff", "oggabog", "moolops" );
        assertEquals( "Given protocol is not blezzaraff", "blezzaraff", testURL.getProtocol() );
    }
    @Test
    public void testTripletURLDomain() {
        URL testURL = new URL( "blezzaraff", "oggabog", "moolops" );
        assertEquals( "Given domain is not oggabog", "oggabog", testURL.getDomain() );
    }
    @Test
    public void testTripletURLPath() {
        URL testURL = new URL( "blezzaraff", "oggabog", "moolops" );
        assertEquals( "Given path is not moolops", "moolops", testURL.getPath() );
    }
}
